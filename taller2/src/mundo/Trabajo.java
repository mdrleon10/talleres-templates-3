package mundo;

import java.util.Date;

public class Trabajo extends Evento
{
	public enum  TipoEventoTrabajo {
		JUNTAS,
		ALMUERZO,
		CENA,
		COCTEL,
		PRESENTACIONES,
		EVENTOS_ESPECIALES,
		CAPACITACION,
		TALLERES,
		ACTIVIDAD_DE_INTEGRACION_Y_BIENESTAR
	}
	
	protected TipoEventoTrabajo tipo;

	public Trabajo(Date pFecha, String pLugar, boolean pObligatorio,
			boolean pFormal, TipoEventoTrabajo pTipo) 
	{
		super(pFecha, pLugar, pObligatorio, pFormal);
		tipo = pTipo;
	}

}
