package mundo;

import java.util.Date;

public class Clientes extends Trabajo
{

	public enum ClientesReunion
	{
		NEGOCIO1,
		NEGOCIO2,
		NEGOCIO3,
		OTROS
	}
	
	protected ClientesReunion tipo;

	public Clientes(Date pFecha, String pLugar, boolean pObligatorio,
			boolean pFormal, TipoEventoTrabajo pTipo, ClientesReunion pNegocioReunion) 
	{
		super(pFecha, pLugar, pObligatorio, pFormal, pTipo);
		tipo = pNegocioReunion;
	}

	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}

	public String toString()
	{
		return "Evento con AMIGOS: \n"
				+"FECHA: " + this.fecha
				+"\nLUGAR: " + this.lugar
				+"\nTIPO EVENTO: " + this.tipo
				+"\nOBLIGATORIO: " + conv(this.obligatorio)
				+"\nFORMAL: "+ conv(this.formal);
	}

}
