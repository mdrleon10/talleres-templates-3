package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.text.html.HTMLEditorKit.Parser;
import javax.xml.bind.ParseConversionEvent;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		Scanner sc1= new Scanner (System.in);
		System.out.println("Número de jugadores: ");
		int numjug = Integer.parseInt(sc1.nextLine());
		ArrayList<Jugador> jugadoresAct = new ArrayList<Jugador>();
		for (int i = numjug; i >=1; i-- )
		{
			System.out.println("Nombre ");
			String nomJug = sc1.nextLine();
			System.out.println("Simbolo ");
			String simJug = sc1.nextLine();

			Jugador jugact = new Jugador(nomJug, simJug);
			jugadoresAct.add(jugact);
		}

		System.out.println("Número de filas: ");
		int numfilas = Integer.parseInt(sc1.nextLine());
		System.out.println("Número de columnas: ");
		int numcol = Integer.parseInt(sc1.nextLine());
		juego = new LineaCuatro(jugadoresAct, numfilas, numcol);
		imprimirTablero();
		juego();
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{

		Scanner sc1= new Scanner (System.in);
		System.out.println(juego.darAtacante());
		System.out.println("Elejir columna ");
		int columna = Integer.parseInt(sc1.nextLine());
		juego.registrarJugada(columna);
		imprimirTablero();
		if (!juego.fin())
		{
			juego();
		}
		else 
			System.out.println("El jugador actual ha gando la partida!");
	}
	
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		Scanner sc1= new Scanner (System.in);
		System.out.println("Nombre del jugador: ");
		String nomJug = sc1.nextLine();
		System.out.println("Simbolo del jugador ");
		String simJug = sc1.nextLine();
		System.out.println("Número de Filas ");
		int filas = Integer.parseInt(sc1.nextLine());
		System.out.println("Número de Columnas ");
		int Columnas = Integer.parseInt(sc1.nextLine());
		Jugador jugadorActual = new Jugador(nomJug, simJug);
		ArrayList<Jugador> jugadoresAct = new ArrayList<Jugador>();
		jugadoresAct.add(jugadorActual);
		Jugador Computador = new Jugador("Computador", "X");
		jugadoresAct.add(Computador);
		juego = new LineaCuatro(jugadoresAct, filas, Columnas);
		imprimirTablero();
		juegoMaquina();
		
		
	}
	
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		Scanner sc1= new Scanner (System.in);
		System.out.println(juego.darAtacante());
		System.out.println("Elejir columna ");
		int columna = Integer.parseInt(sc1.nextLine());
		juego.registrarJugada(columna);
		juego.registrarJugadaAleatoria();
		imprimirTablero();
		if (!juego.fin())
		{
			juegoMaquina();
		}
		else 
			System.out.println("El jugador actual ha gando la partida!");
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String tab = "";
		for (int i = 0; i < juego.darTablero().length; i++)
		{
			for (int j = 0; j < juego.darTablero()[i].length; j++)
			{
				tab+=juego.darTablero()[i][j]+ "|";
			}
			tab+="\n";
		}
		System.out.println(tab);
	}
}
