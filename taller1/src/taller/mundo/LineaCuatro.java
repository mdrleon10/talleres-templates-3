package taller.mundo;

import java.util.ArrayList;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;

	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		int col = (int)(Math.random()*tablero[0].length);
		boolean jugadaexitosa = registrarJugada(col);

		while (!jugadaexitosa)
		{
			col = (int)(Math.random()*tablero[0].length);
			jugadaexitosa = registrarJugada(col);
		}
	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		boolean jugada = false;
		for (int i = 0; i < tablero.length&& jugada !=true; i++) 
		{

			if (i+1< tablero.length &&tablero[i+1][col]== "___")
			{
				tablero[i][col]= jugadores.get(turno).darSimbolo();
				jugada = true;

				if (terminar(i, col)== true)
				{
					finJuego = true;
				}
			}

		}

		if (turno +1< jugadores.size())
		{
			turno ++;
		}
		else 
		{
			turno = 0;
		}

		atacante = jugadores.get(turno).darNombre();
		return jugada;
	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{

		// col
		String jugada= tablero[fil][col];
		int contador=0;
		for (int i = fil; i < tablero[col].length; i++) {
			String pos=tablero[i][col];
			if ( pos.equals(jugada))
				contador++;
			else
				contador=0;
			if(contador>3)
				return true;

		}
		//fil

		contador=0;
		for (int i = 0; i < tablero.length; i++) {
			String pos=tablero[fil][i];
			if ( pos.equals(jugada))
				contador++;
			else 
				contador=0;
			if(contador>3)
				return true;

		}
		//diag
		contador=0;
		boolean para=false;
		boolean para2=false;
		for (int i = 0; i < tablero.length && !para; i++) {
			try
			{
				String pos=tablero[fil+1][col+1];
				if ( pos.equals(jugada))
					contador++;
				else
					para=true;
			}
			catch(Exception e)
			{
				para=true;
			}
		}
		for (int i = 0; i < tablero.length && !para2; i++) {
			try
			{
				String pos2=tablero[fil-1][col-1];
				if ( pos2.equals(jugada))
					contador++;
				else 
					para2=false;
			}

			catch(Exception e)
			{
				para2=true;
			}

			if (contador>3)
				return true;


		}

		return false;

	}
}




